#include "MainWindow.h"
#include "ui_mainwindow.h"
#include <chrono>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <thread>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	CPUSorgu();
	CPUModel();
	MemoryRead();
	QString cpu;
	CPU(cpu, _qle, NUM_CPU_STATES);
	CreateMemoryTab(_qlm);
	CreateTemperatureTab(_qlt);
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()),this, SLOT(cpudegeryaz()));
	connect(timer, SIGNAL(timeout()),this, SLOT(MemoryRead()));
	connect(timer, SIGNAL(timeout()),this, SLOT(TemperatureRead()));
	timer -> start(1500);
}

/********************************************************/
//						MEMORY TAB						//
/********************************************************/

void MainWindow::CreateMemoryTab(QList<QLineEdit *> &qlm)
{
	auto _memory_tab = this->findChild<QWidget*>("memory_tab" , Qt::FindChildrenRecursively);
	QGridLayout *vbl = new QGridLayout(_memory_tab);

	for(int i=0; i<3; i++){
		auto *e = new QLineEdit();
		auto *d = new QLabel();
		e->isReadOnly();
		if ( i == 0) {
			d->setText("Toplam Hafıza : ");
		}
		if ( i == 1) {
			d->setText("Boş Hafıza : ");
		}
		if ( i == 2) {
			d->setText("Kullanılan Hafıza Oranı : ");
		}
		qlm.append(e);
		qlm.at(i)->setObjectName("Ram" + QString::number(i));
		vbl->addWidget(e,i,1);
		vbl->addWidget(d,i,0);
	}
}

void MainWindow::MemoryRead()
{
	QString name = ("/proc/meminfo");
	QFile data(name);
	if(!(data.open(QFile::ReadOnly))){
		qDebug()<<"Bu dosya veya konumu acilamiyor";
	}
	QTextStream line(&data);
	for(int i=0 ; i < 3  ; i++){
		QString deger = line.readLine();
		QString y = deger.right(11);
		y.remove("kB",Qt::CaseSensitive);
		float a =y.toInt();
		float toplam;
		float	kullanilan;
		if(_qlm.size()==3){
			if (i==0){
				toplam = a;
				int x = a/1000000;
				int b = a/100000;
				_qlm[0]->setText(QString::number(x) + "," + QString::number(b) + " GB");
			}
			if(i==1){
				kullanilan = a;
				int x = a/1000000;
				int b = a/10000;
				_qlm[1]->setText(QString::number(x) + "," + QString::number(b) + " GB");
			}
			if(i==2){
				float yuzde = toplam/kullanilan*10;
				_qlm[2]->setText("%" + QString::number( yuzde));
			}
		}
	}
}

/********************************************************/
//					TEMPERATURE TAB						//
/********************************************************/

void MainWindow::CreateTemperatureTab(QList<QLineEdit *> &qlt)
{
	auto _temperature_tab = this->findChild<QWidget*>("temperature_tab" , Qt::FindChildrenRecursively);
	QGridLayout *vblt = new QGridLayout(_temperature_tab);

	for (int i = 0 ; i<3 ; i++){
		auto *e = new QLineEdit();
		auto *d = new QLabel();
		d->setText("Sensor " + QString::number(i));
		e->isReadOnly();
		qlt.append(e);
		qlt.at(i)->setObjectName("Temp" + QString::number(i));
		vblt->addWidget(e,i,1);
		vblt->addWidget(d,i,0);
	}
}

void MainWindow::TemperatureRead()
{
	QString name = ("/sys/class/hwmon/hwmon5");
	for (int i=1 ; i<4 ;i++){
		QString filename = name +"/temp"+ QString::number(i)+ "_input";
		QFile data(filename);
		if(!data.open((QFile::ReadOnly))){
			qDebug() <<"Sıcaklık icin" <<i<<" dosya veya konumu acilamiyor";
		}
		QTextStream in (&data);
		QString deger = in.readLine();
		deger.resize(2);
		_qlt[i-1]->setText(deger + " Celcius");
		_qlt.at(i-1)->setObjectName("temp"+QString::number(i));
		data.close();
	}
}

/********************************************************/
//						CPU TAB							//
/********************************************************/

enum CPUStates
{
	S_USER = 0,
	S_NICE,
	S_SYSTEM,
	S_IDLE,
	S_IOWAIT,
	S_IRQ,
	S_SOFTIRQ,
	S_STEAL,
	S_GUEST,
	S_GUEST_NICE
};

struct CPUData{
	std::string cpu;
	QVector<std::size_t> times;
};

size_t GetIdleTime(const CPUData & e)
{
	return  e.times[S_IDLE] +
			e.times[S_IOWAIT];
}

size_t GetActiveTime(const CPUData & e)
{
	return  e.times[S_USER] +
			e.times[S_NICE] +
			e.times[S_SYSTEM] +
			e.times[S_IRQ] +
			e.times[S_SOFTIRQ] +
			e.times[S_STEAL] +
			e.times[S_GUEST] +
			e.times[S_GUEST_NICE];
}

void MainWindow::ReadStatsCPU(QVector<CPUData> &entries)
{
	std::ifstream fileStat("/proc/stat");
	std::string line;
	const std::string STR_CPU("cpu");
	const std::string STR_TOT("tot");
	const std::size_t LEN_STR_CPU = STR_CPU.size();
	while(std::getline(fileStat, line)){
		//cpu kelimesi ile baslayan satirlarin bulunması icin
		//compare kullanıldı. 0. karakterden itibaren, LEN_STR_CPU
		//kadar karakter STR_CPU ile karşılaştırılması anlamına gelir.
		if(!line.compare(0, LEN_STR_CPU, STR_CPU)){
			std::istringstream ss(line);
			//vectorname.emplace_back(value)
			entries.append(CPUData());
			CPUData & entry = entries.back();
			ss >> entry.cpu;
			if(entry.cpu.size() > LEN_STR_CPU){
				entry.cpu.erase(0, LEN_STR_CPU);
			}
			else{
				entry.cpu = STR_TOT;
			}
			for(int i = 0; i < NUM_CPU_STATES; ++i){
				size_t value;
				ss >> value;
				entry.times.append(value);
			}
		}
	}
}

void MainWindow::PrintStats(const QVector<CPUData> & entries1, const QVector<CPUData> & entries2)
{
	if (entries1.size() != entries2.size())
		return;
	for(int x=0 ; x<NUM_CPU_STATES; x++){
		QString str;
		const CPUData & e1 = entries1[x];
		const CPUData & e2 = entries2[x];
		const float ACTIVE_TIME	= static_cast<float>(GetActiveTime(e2) - GetActiveTime(e1));
		const float IDLE_TIME	= static_cast<float>(GetIdleTime(e2) - GetIdleTime(e1));
		const float TOTAL_TIME	= (ACTIVE_TIME + IDLE_TIME);
		float active = (100.f * ACTIVE_TIME / TOTAL_TIME);
		float idle = (100.f * IDLE_TIME / TOTAL_TIME);

		QTextStream ss(&str);
		QString son;
		if (x<10){
			son="CPU"+QString::number(x)+"  Active: %"+QString::number(active)+"    İdle: %"+QString::number(idle);
		}
		else{
		son="CPU"+QString::number(x)+"  Active: %"+QString::number(active)+"  İdle: %"+QString::number(idle);
		}
		ss << qSetRealNumberPrecision(2) <<son;
		_qle[x]->setText(str);
	}
}

void MainWindow::cpudegeryaz()
{
	QVector<CPUData> entries1;
	QVector<CPUData> entries2;
	ReadStatsCPU(entries1);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	ReadStatsCPU(entries2);
	PrintStats(entries1, entries2);
}

void MainWindow::CPU(QString label, QList<QLabel *> &qle,int labelsayisi)
{
	auto _cpu_tab = this->findChild<QWidget*>("cpu_tab" , Qt::FindChildrenRecursively);
	QVBoxLayout *hbl = new QVBoxLayout(_cpu_tab);

	for(int i=0; i<labelsayisi+4; i++){
		auto *e = new QLabel();
		e->setText(label+QString::number(i));
		qle.append(e);
		qle.at(i)->setObjectName(label+QString::number(i));
		if(i==labelsayisi+1){
			e->setText("İşlemci Adı:                         " + _qls.at(0));
		}
		if(i==(labelsayisi+2)){
			e->setText("Fiziksel Çekirdek Sayısı:   " + _qls.at(1));
		}
		if(i==(labelsayisi+3)){
			e->setText("Üretici:                                " + _qls.at(2));
		}
		if(i==(labelsayisi)){
			e->setText("");
		}
		hbl->addWidget(e);
	}
}

void MainWindow::CPUModel(){
	QHash <QString, QString> stat;
	QString name = ("/proc/cpuinfo");
	QFile data(name);
	if(!(data.open(QFile::ReadOnly))){
		qDebug()<<"Bu dosya veya konumu acilamiyor";
	}
	QTextStream line(&data);
	for(int i=0 ; i < 5 ; i++){
		QString deger = line.readLine();
		deger.remove("\t");
		stat.insert(deger.split(":").first(), deger.split(":").last());
	}
	QString model = stat.value("model name");
	QString core = stat.value("cpu family");
	QString brand = stat.value("vendor_id");
	_qls.append(model);
	_qls.append(core);
	_qls.append(brand);
}

int MainWindow::NUM_CPU_STATES = 0;

void MainWindow::CPUSorgu()
{
	QString loc = ("/proc/stat");
	QFile data(loc);
	if(!(data.open(QFile::ReadOnly))){
		qDebug()<<"Bu dosya veya konumu acilamiyor";
	}
	QTextStream line(&data);
	int c =0;
	for(int i = 0 ; i<15 ; i++){
		QString deger = line.readLine();
		bool eng = deger.contains("cpu",Qt::CaseInsensitive);
		if( eng ) c++;
		NUM_CPU_STATES = c;
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}
