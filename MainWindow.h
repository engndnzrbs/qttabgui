#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>

namespace Ui {
class MainWindow;
}
struct CPUData;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	void CPUSorgu();
	static int NUM_CPU_STATES ;
	QList<QLabel *> _qle;
	QList<QLineEdit *> _qlm;
	QList<QLineEdit *> _qlt;
	QTimer *timer;
	QList<QString > _qls;

private slots:
	void cpudegeryaz();
	void MemoryRead();
	void TemperatureRead();

private:
	Ui::MainWindow *ui;
	void CPUModel();
	void ReadStatsCPU(QVector<CPUData> &entries);
	void PrintStats(const QVector<CPUData> & entries1, const QVector<CPUData> & entries2);
	void CPU(QString label, QList<QLabel *> &qle, int labelsayisi);
	void CreateMemoryTab(QList<QLineEdit *> &qlm);
	void CreateTemperatureTab(QList<QLineEdit *> &qlt);
};
#endif // MAINWINDOW_H
