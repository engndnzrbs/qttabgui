#ifndef NEWBUTON_H
#define NEWBUTON_H

#include <QtWidgets>

class NewButon : public QWidget
{
	Q_OBJECT
public:
	explicit NewButon(QWidget *parent = nullptr);
	void ButonOlustur();
private:
	QPushButton *m_buton;
signals:

public slots:
};

#endif // NEWBUTON_H
